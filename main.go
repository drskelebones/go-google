package main

import (
	"strings"
	"fmt"
	"os"
	
	"github.com/rocketlaunchr/google-search"
	. "github.com/logrusorgru/aurora/v3"
)

func main() {
	query := strings.Join(os.Args[1:len(os.Args)], " ")
	var opts googlesearch.SearchOptions
	opts.Limit = 6
	results, err := googlesearch.Search(nil, query, opts)
	if err != nil {
		fmt.Println("Error searching. Possibly an issue with Google.")
	}
	fmt.Printf("%s '%s' ::\n", Magenta("Results for"), query)
	if len(results) > 0 {
		for i := 0; i < len(results); i++ {
			fmt.Printf("%d : %s \n--: %s\n", Blue(results[i].Rank), Magenta(results[i].Title), results[i].URL)
		}
	} else {
		fmt.Println("Nothing found for '" + query + "'!")
	}
}
