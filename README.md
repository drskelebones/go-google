# go-google
Quick and dirty command for searching Google and getting the top five results.

![demo](https://cdn.skele.dev/grotesque-ignorant-unlined-fake-milksnake/direct)

## Uses
- [google-search](https://github.com/rocketlaunchr/google-search) - nice module for scraping results from Google easily.
- [aurora](https://github.com/logrusorgru/aurora) - colors!

## Build
`go build -o google`

## Install
`cp google /usr/local/bin/.`